/**
 * @file
 * Initialized jQueryUI tabs for translatable fields.
 */

(function($){
  Drupal.behaviors.entityTranslationSinglePage = {
    attach: function(context) {
      $('.entity-translation-tabs', context).once().tabs();
    },
    detach: function(context) {
      $('.entity-translation-tabs', context).once().tabs('destroy');
    }
  }
})(jQuery);
