CONTENTS OF THIS FILE
---------------------

 * Description
 * Installation
 * Limitations


DESCRIPTION
------------

The Entity Translation Single Page module provides single screen for editing
multilingual nodes: switch between different languages using jQuery UI Tabs.


INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.


LIMITATIONS
-----------
1. Module is not integrated with Entity Translation, i.e. if Multilingual
support will be changed to 'Enabled, with field translation' (provided by the
Entity Translation module), no translations to other languages will be present.
But the field translation itself will be preserved. Basically, when new
translation will be added, field will have translated values (not a default
language ones)
2. Module supports nodes only
3. Nodes will be created in current content language (neutral language breaks
this module functionality)
4. jQuery UI Tabs is not aware of form validation errors, i.e. first tab is
always being displayed as default
5. Data loses when field translation status switched back & forth. All
translations, except for node language will be lost (actually they will stay
in the DB, but without any UI access to that data)
